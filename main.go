package main

import (
	"errors"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
)

// getCharWeight
// disini saya menggunakan rune sebagai alternatif penghitungan karakter string
// misal karakter a bernilai karakter ascii 97
// jadi a = 97 - 97 + 1 = 1, dan seterusnya
func getCharWeight(char rune) int {
	// solusi lain bisa menggunakan index karakter contoh
	// alphabet := "abcdefghijklmnopqrstuvwxyz"
	// index := strings.Index(alphabet, char)
	// c := index + 1
	c := int(char - 'a' + 1)
	return c
}

// penjumlahan string menjadi bobot sesuai besar angka urutannya
// jika ada karakter berulang berurutan maka akan ditambah dengan karakter yang sama
// misal aa = 1+1, bb = 2+2 dst
// lalu bobot karakter yang telah dijumlah akan ditampung kedalam hash map sebagai key dan value true agar memudahkan pencarian
// contoh string aabb maka akan di simpan [1:true,2:true,4:true]
func generateSubstringsWeights(s string) map[int]bool {
	weights := make(map[int]bool)
	n := len(s)

	// looping sesuai jumlah karakter dalam string
	for i := 0; i < n; {
		weight := getCharWeight(rune(s[i]))
		weights[weight] = true
		j := i + 1 // variable j akan di tambah dengan i karena sebagai index ketika ada karakter yang sama
		currentWeight := weight
		y := string(s[i])
		log.Printf("char %s bobot %d ", y, currentWeight)
		for j < n && s[j] == s[i] { // pengecekan jika ada karakter yang sama berurutan
			currentWeight += weight // bobot karakter akan ditambah dengan bobot karakter yang sama
			y += y
			log.Printf("char %s bobot %d ", y, currentWeight)
			weights[currentWeight] = true
			j++
		}

		i = j
	}

	return weights
}

// fungsi untuk mencocokan bobot per karakter dalam string dengan nilai array yang di tentukan
func weightedStrings(s string, queries []int) []string {
	weights := generateSubstringsWeights(s)
	results := make([]string, len(queries))

	for i, query := range queries {
		if weights[query] { // jika jumlah bobot karakter sama dengan nilai int maka return "Yes"
			results[i] = "Yes"
		} else {
			results[i] = "No"
		}
	}

	return results
}

func main() {
	var inputStr string
	var inputQueries string

	fmt.Println("Masukkan string alfabet (default: abbcccd)")
	fmt.Scanln(&inputStr)

	// set default
	if inputStr == "" {
		inputStr = "abbcccd"
		fmt.Println("input tidak di isi, akan di isi dengan default")
	}
	inputStr = strings.ToLower(inputStr)
	inputStr = strings.TrimSpace(inputStr)
	inputStr = strings.ReplaceAll(inputStr, " ", "")

	fmt.Println("Masukkan array integer dalam format [angka1,angka2,...,angkaN]:  (default: [1, 3, 9, 8])")
	fmt.Scanln(&inputQueries)

	// set default
	if inputQueries == "" {
		inputQueries = "1,3,9,8"
		fmt.Println("input tidak di isi, akan di isi dengan default")
	}
	inputQueries = strings.TrimSpace(inputQueries)
	inputQueries = strings.ReplaceAll(inputQueries, " ", "")
	err := validateInput(inputStr, inputQueries)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	queries, err := convertStrToArrayInt(inputQueries)
	if err != nil {
		return
	}

	results := weightedStrings(inputStr, queries)

	fmt.Println(results)
}

func validateInput(inputStr, inputQueries string) error {
	regexStr := `[a-z]`
	regexQueries := `^[0-9,]+$`
	regStr, _ := regexp.Compile(regexStr)
	regQry, _ := regexp.Compile(regexQueries)
	switch false {
	case regStr.MatchString(inputStr):
		return errors.New("hanya diperbolehkan alfabet a-z")
	case regQry.MatchString(inputQueries):
		return errors.New("hanya diperbolehkan angka dengan koma sebagai pemisah, contoh: 1,2,3")
	}
	return nil
}

func convertStrToArrayInt(inputQueries string) ([]int, error) {
	stringValues := strings.Split(inputQueries, ",")

	// Buat slice untuk menampung integer
	intValues := make([]int, 0, len(stringValues))
	for _, v := range stringValues {
		if v == "" {
			continue
		}
		intValue, err := strconv.Atoi(v)
		if err != nil {
			fmt.Println("Error converting to integer:", err)
			return nil, err
		}
		intValues = append(intValues, intValue)
	}
	return intValues, nil
}
